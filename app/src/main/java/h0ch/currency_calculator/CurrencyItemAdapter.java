package h0ch.currency_calculator;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by h0ch on 09.06.2016.
 */
public class CurrencyItemAdapter extends BaseAdapter {
    private int curID;
    private int flagID;
    private int addInfoID;

    private boolean capitalsFlag;
    private boolean exchangeRateFlag;

    public CurrencyItemAdapter(int curID,
                               int flagID,
                               int addInfoID,
                               boolean capitals,
                               boolean exchangeRate ) {
        this.curID = curID;                 // ID of TextView for currency name
        this.flagID = flagID;               // ID of ImageView for flag
        this.addInfoID = addInfoID;         // ID of TextView for capital name and exchange rate (additional info)

        this.capitalsFlag = capitals;           // determines if Capitals are shown or not.
        this.exchangeRateFlag = exchangeRate;   // determines if Exchange Rates are shown or not.
    }

    @Override
    // delivers amount of entries in currency-list from ExchangeRateDatabase
    public int getCount() { return CalculatorActivity.db.getCurrencies().length; }

    @Override
    // delivers currency with selected position
    public Object getItem(int position) { return CalculatorActivity.db.getCurrencies()[position]; }

    @Override
    // delivers currency with selected position (ID)
    public long getItemId(int position) { return position; }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Context context = parent.getContext();
        String currencyName = CalculatorActivity.db.getCurrencies()[position];
        String capitalName = CalculatorActivity.db.getCapital(currencyName);
        String exchangeRate = Double.toString(CalculatorActivity.db.getExchangeRate(currencyName));

        LayoutInflater inflater =
                (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View root = convertView;
        if(root == null){
            root = inflater.inflate(R.layout.cur_view_item, null, false);
        }

        // set currenyName to Textview:
        TextView cur_name = (TextView) root.findViewById(curID);
        cur_name.setText(currencyName);

        // get Filename + Path of flag via chosen currency
        ImageView cur_flag = (ImageView) root.findViewById(flagID);
        String cur_flag_path = "flag_".concat( currencyName.toLowerCase() );

        // get ImageID via filename in "drawable"
        int imageID = context.getResources().getIdentifier(
                cur_flag_path, "drawable", context.getPackageName());

        // Image view; set image via ID:
        cur_flag.setImageResource(imageID);

        // return here already when no additional info has to be displayed
        if(!capitalsFlag && !exchangeRateFlag){
            return root;
        }

        TextView view_add_info = (TextView) root.findViewById(addInfoID);
        view_add_info.setText("[ ");

        // set capital
        if(capitalsFlag){
            view_add_info.append(capitalName);
        }

        if(capitalsFlag && exchangeRateFlag){
            view_add_info.append(" | ");
        }

        // set exchange rate
        if(exchangeRateFlag){
            view_add_info.append(exchangeRate);
        }
        view_add_info.append(" ]");

        return root;
    }
}
