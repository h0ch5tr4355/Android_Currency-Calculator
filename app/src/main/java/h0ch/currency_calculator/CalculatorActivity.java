package h0ch.currency_calculator;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;

public class CalculatorActivity extends AppCompatActivity {

    public static ExchangeRateDatabase db = new ExchangeRateDatabase();

    private ShareActionProvider shareActionProvider;

    private Spinner spinnerFrom;
    private Spinner spinnerTo;
    private EditText valEdit;
    private TextView resultView;
    private Button buttonCalc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Allow accessing GUI
        //StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        //StrictMode.setThreadPolicy(policy);

        // custom Adapter for Currencies
        CurrencyItemAdapter curSpinnerAdapter = new CurrencyItemAdapter(
                R.id.cur_view,
                R.id.cur_flag,
                R.id.add_info_view,
                true,
                false
                );

        // Spinner "From value"
        spinnerFrom = (Spinner) findViewById(R.id.spinner_from);
        spinnerFrom.setAdapter(curSpinnerAdapter);
        spinnerFrom.setSelection(8); //Euro as Default <FROM> //ID8

        // Spinner "To value"
        spinnerTo = (Spinner) findViewById(R.id.spinner_to);
        spinnerTo.setAdapter(curSpinnerAdapter);
        spinnerTo.setSelection(30); //USD as Default <TO> // ID30

        valEdit = (EditText) findViewById(R.id.val_edit);
        resultView = (TextView) findViewById(R.id.result_view);

        buttonCalc = (Button) findViewById(R.id.button_calc);
        buttonCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcCurrencies();
            }
        });
        // loading stored currencies
        restoreCurrencies();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_calculator, menu);

        MenuItem shareItem = menu.findItem(R.id.action_share);
        shareActionProvider = (ShareActionProvider)
                MenuItemCompat.getActionProvider(shareItem);

        // Share-Text when CALC-Button has not pushed yet
        setShareText(
                getResources().getString(R.string.share_line_default_start) + "!\n" +
                        getResources().getString(R.string.share_line_default_end) + ":\n" +
                        getResources().getString(R.string.play_store_url));
        return true;
    }

    private void setShareText(String text) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        if (text != null) {
            shareIntent.putExtra(Intent.EXTRA_TEXT, text);
        }
        shareActionProvider.setShareIntent(shareIntent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_currency_list) {
            Intent currencyListIntent = new Intent(CalculatorActivity.this, CurrencyListActivity.class);
            startActivity(currencyListIntent);
        }

        return super.onOptionsItemSelected(item);
    }

    public void calcCurrencies() {
        Log.i("LOG: Waehrungsrechner", "Calculate Button clicked");

        spinnerFrom = (Spinner) findViewById(R.id.spinner_from);
        spinnerTo = (Spinner) findViewById(R.id.spinner_to);

        TextView errorTextView = (TextView) findViewById(R.id.textview_val_input_error);
        errorTextView.setText("");

        if(valEdit.getText().toString().matches("")) {
            errorTextView.setText(getResources().getString(R.string.error_no_input).toUpperCase());
            resultView.setText("");
            return;
        }

        String curFrom = spinnerFrom.getSelectedItem().toString();
        String curTo = spinnerTo.getSelectedItem().toString();
        String insertedValue = valEdit.getText().toString();

        // calculation: 1st currency to 2nd currency
        DecimalFormat df = new DecimalFormat("0.00");
        String result = df.format( db.convert(Double.parseDouble(insertedValue), curFrom, curTo) );

        resultView.setText(result);

        // Generate Text for Share Button
        setShareText(
                getResources().getString(R.string.app_name) + " " +
                getResources().getString(R.string.share_line_say) + ":\n" +
                df.format(Double.parseDouble(insertedValue)) + " " + curFrom + " " +
                getResources().getString(R.string.share_line_are) + " " +
                result + " " + curTo + " !");
    }

    /*
     * restore currencies
     */
    private void restoreCurrencies() {

        FileInputStream fin = null;
        int c;
        String data="";
        /*
        * Currencies stored in format: "<cur>,<rate>;<cur>,<rate>;..."
        * --> 1. Splitting into cur-rate-pairs at ";"
        *     2. Splitting at pairs at ","
        */
        String[] currencies;
        String[] currency;
        try {
            fin = openFileInput("exchange_rates");
            while( (c = fin.read()) != -1){
                data = data + Character.toString((char)c);
            }
            fin.close();
            currencies = data.split(";");

            // 32 currencies:
            Log.i("currencies[] length", String.valueOf(currencies.length));

            for(int i=0; i < currencies.length; i++) {
                currency = currencies[i].split(",");
                Log.i( "Currency found " + i, currency[0] + currency[1] );
                ExchangeRate r = new ExchangeRate(currency[0],
                        CalculatorActivity.db.getCapital(currency[0]),
                        Double.parseDouble(currency[1]));
                CalculatorActivity.db.setExchangeRate(r);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.i("restore Currencies", data);
    }

    @Override
    /* Save values of last selected items of spinnerFrom, spinnerTo, valEdit and resultView */
    protected void onPause() {
        super.onPause();
        SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();

        int spinnerFromValue = spinnerFrom.getSelectedItemPosition();
        editor.putInt("SpinnerFrom", spinnerFromValue);

        int spinnerToValue = spinnerTo.getSelectedItemPosition();
        editor.putInt("SpinnerTo", spinnerToValue);

        String valEditString = valEdit.getText().toString();
        editor.putString("valEdit", valEditString);

        //String resultViewString = resultView.getText().toString();
        //editor.putString("resultView", resultViewString);

        Log.i("CalculatorActivity", "onPause() called");

        editor.apply();
    }

    @Override
    /* Restore values of last selected items of spinnerFrom, spinnerTo, valEdit and resultView */
    protected void onResume() {
        super.onResume();
        SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);

        int spinnerFromValue = prefs.getInt("SpinnerFrom", 8); //Euro as Default <FROM> //ID8
        spinnerFrom.setSelection(spinnerFromValue);

        int spinnerToValue = prefs.getInt("SpinnerTo", 30); //USD as Default <TO> // ID30
        spinnerTo.setSelection(spinnerToValue);

        String valEditString = prefs.getString("valEdit", ""); // Empty EditText as Default
        valEdit.setText(valEditString);

        //String resultViewString = prefs.getString("resultView", null); // Empty Label as Default
        //resultView.setText(resultViewString);
    }
}
