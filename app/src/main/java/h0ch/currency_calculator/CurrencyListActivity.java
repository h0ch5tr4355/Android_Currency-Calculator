package h0ch.currency_calculator;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class CurrencyListActivity extends AppCompatActivity {

    ExchangeRateUpdateRunnable runnable = null;
    String selectedCurrency;
    int selectedPosition;
    CurrencyItemAdapter curListViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_currency_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        curListViewAdapter = new CurrencyItemAdapter(
                R.id.cur_view,
                R.id.cur_flag,
                R.id.add_info_view,
                true,
                true);

        final ListView curListView = (ListView) findViewById(R.id.view_cur_list);
        curListView.setAdapter(curListViewAdapter);

        /*
         * Short Click on Currency-List item opens Google Maps
         */
        curListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selectedCapital = CalculatorActivity.db.getCapital(
                        (String)curListViewAdapter.getItem(position));
                Log.i("onItemClick", (selectedCapital));
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("geo:0,0`?q=" + selectedCapital));
                if(intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }
            }
        });

        /*
         * Long Click on Currency-List item opens an Edit-Activity for the currency
         */

        // Yes / No Dialog for AlertBuilder (Edit Currency --> Yes/No)
        final DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        Log.i("DialogInterface", "yes clicked");
                        Intent detailsIntent = new Intent(CurrencyListActivity.this,
                                EditExchangeRate.class);
                        detailsIntent.putExtra("newExchangeRate", selectedCurrency);
                        startActivityForResult(detailsIntent, selectedPosition);
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        Log.i("DialogInterface", "no clicked");
                        break;
                }
            }
        };
        // LongClickListener
        curListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                String selectedCapital = CalculatorActivity.db.getCapital(
                        (String)curListViewAdapter.getItem(position));
                selectedCurrency = (String)curListViewAdapter.getItem(position);
                selectedPosition = position;

                Log.i("onItemLongClick", (selectedCapital));
                new AlertDialog.Builder(CurrencyListActivity.this)
                        .setTitle(getResources().getString(R.string.cur_edit_title).toUpperCase())
                        .setMessage(getResources().getString(R.string.cur_edit_ask1) +
                                " " + selectedCurrency + " " +
                                getResources().getString(R.string.cur_edit_ask2) + "?")
                        .setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener)
                        .setIcon(R.mipmap.ic_launcher)
                        .show();
                return true;
            }
        });
    }

    // method back from EditExchangeRate Activity
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("CurrencyListActivity", "onActivityResult() called");
        Log.i("CurrencyListActivity", "selected Currency " + selectedCurrency );

        Bundle extras = null;
        try {
            extras = data.getExtras();
        } catch (NullPointerException e){
            e.printStackTrace();
            Log.i("onActivityResult", "no data returned");
            return;
        }

        if (extras != null) {
            Double value = extras.getDouble("newExchangeRate");
            Log.i("CurrencyListActivity", "returned Double " + value );
            CalculatorActivity.db.setExchangeRate(new ExchangeRate(
                                                selectedCurrency,
                                                CalculatorActivity.db.getCapital(selectedCurrency),
                                                value));
            storeCurrencies();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_currency_list, menu);
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        storeCurrencies();
        Log.i("CurrencyListActivity", "onPause() called");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_help_currency) {
            Log.i("Currency List", "\"Key\" selected");
            new AlertDialog.Builder(this)
                    .setTitle(getResources().getString(R.string.key_title))
                    .setMessage(getResources().getString(R.string.key_subtitle) + "\n" +
                            "• " + getResources().getString(R.string.key_line1_flag) + "\n" +
                            "• " + getResources().getString(R.string.key_line2_curname) + "\n" +
                            "• " + getResources().getString(R.string.key_line3_cap) + "\n" +
                            "• " + getResources().getString(R.string.key_line4_rate) + "\n" + "\n" +
                            getResources().getString(R.string.key_line5_short) + "\n" +
                            "• " + getResources().getString(R.string.key_line6_short_descr) + "\n" +
                            getResources().getString(R.string.key_line7_long) + "\n" +
                            "• " + getResources().getString(R.string.key_line8_long_descr))
                    .setIcon(R.drawable.key_icon)
                    .show();
        }

        /*
         * Update Currencies in Background with Thread
         */
        if (id == R.id.action_refresh_currency) {
            Log.i("Currency List", "\"Update currencies\" selected");

            runnable = new ExchangeRateUpdateRunnable(this, (ListView) findViewById(R.id.view_cur_list));
            new Thread(runnable).start();
        }

        return super.onOptionsItemSelected(item);
    }

    private void storeCurrencies(){
        // Storing of currencies:
        String data = "";
        try {
            FileOutputStream fOut = openFileOutput("exchange_rates", Context.MODE_PRIVATE);
            for (String s : CalculatorActivity.db.getCurrencies()) {
                data = data + s + "," + CalculatorActivity.db.getExchangeRate(s) + ";";
            }
            fOut.write(data.getBytes());
            fOut.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.i("Storing currencies", data);
    }
}
