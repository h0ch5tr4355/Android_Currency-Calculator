package h0ch.currency_calculator;


import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by h0ch on 03.07.2016.
 */
public class ExchangeRateUpdateRunnable implements Runnable {

    private Activity activity;
    private ListView listView;
    private CurrencyItemAdapter adapter;

    public ExchangeRateUpdateRunnable(Activity activity,View view) {
        this.activity = activity;
        this.listView = (ListView) view;
        adapter = (CurrencyItemAdapter) listView.getAdapter();
    }

    @Override
    public void run() {
        Log.i("UpdateThread", "Started Update");
        if(updateCurrencies()) {
            updateList();
            showFinishToast();
            Log.i("UpdateThread", "Finished Update");
        } else {
            showUpdateFalToast();
            Log.i("UpdateThread", "Update not sucessful, no Network access");
        }
    }

    /*
     * Refresh ListView
     */
    synchronized private void updateList() {
        listView.post(new Runnable() {
            @Override
            public void run() {
                synchronized (ExchangeRateUpdateRunnable.this) {
                    // http://stackoverflow.com/questions/2250770/how-to-refresh-android-listview
                    adapter.notifyDataSetChanged();
                }
            }
        });
    }

    synchronized private void showFinishToast() {
        final String msg = activity.getResources().getString(R.string.toast_cur_update);
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast toast = Toast.makeText(activity, msg ,Toast.LENGTH_SHORT);
                toast.show();
            }
        });
    }

    synchronized private void showUpdateFalToast() {
        final String msg = activity.getResources().getString(R.string.toast_cur_update_fail);
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast toast = Toast.makeText(activity, msg ,Toast.LENGTH_SHORT);
                toast.show();
            }
        });
    }

    /*
     * Update Currencies: Returns True when successful
     */
    private boolean updateCurrencies() {
        String urlStringEcb = "https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";

        try {
            URL url = new URL(urlStringEcb);
            URLConnection connection = url.openConnection();

            XmlPullParser parser = XmlPullParserFactory.newInstance().newPullParser();
            parser.setInput(connection.getInputStream(), connection.getContentEncoding());

            int eventType = parser.getEventType();

            int counter = 0;

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {
                    if ("Cube".equals(parser.getName())) {
                        if (parser.getAttributeValue(null, "currency") != null) {
                            counter++;
                            String cur = parser.getAttributeValue(null, "currency");
                            Double rate = Double.parseDouble(parser.getAttributeValue(null, "rate"));
                            //Log.i("Node " + counter, "Currency: " + cur);
                            //Log.i("Node " + counter, "Rate: " + rate.toString());
                            ExchangeRate r = new ExchangeRate(cur,
                                    CalculatorActivity.db.getCapital(cur),
                                    rate);
                            CalculatorActivity.db.setExchangeRate(r);
                        }
                    }
                }
                eventType = parser.next();
            }
        } catch (Exception e) {
            Log.e("ECB Access", "Failed to access data!");
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
