package h0ch.currency_calculator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class EditExchangeRate extends AppCompatActivity {

    EditText exchangeRateEdit;
    TextView errorView;
    Button abortButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_exchange_rate);

        exchangeRateEdit = (EditText) findViewById(R.id.edit_exchangerate_view);
        errorView = (TextView) findViewById(R.id.edit_exchangerate_error_view);
        abortButton = (Button) findViewById(R.id.edit_abort_button);

        abortButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("EditExchangeRate", "Abort Button clicked!");
                finishActivity();
            }
        });

        exchangeRateEdit.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                // Keyboard "tick" has to be set (XML: android:imeOptions="actionDone" )
                if(actionId == EditorInfo.IME_ACTION_DONE) {
                    Intent returnIntent = new Intent();

                    //try if EditText is castable to double, else give an error in TextView
                    try {
                        Double.parseDouble(exchangeRateEdit.getText().toString());
                    } catch (NumberFormatException e){
                        errorView.setText(getResources().getString(R.string.cur_edit_error_input));
                        Log.i("onEditorActionListener", e.getMessage());
                        return false;
                    }
                    //return casted Double from EditText
                    returnIntent.putExtra("newExchangeRate",
                            Double.parseDouble(exchangeRateEdit.getText().toString()));
                    setResult(RESULT_OK, returnIntent);
                    finish();
                    Log.i("onEditActionListener", "return true");
                    return true;
                }
                Log.i("onEditActionListener", "return false");
                return false;
            }
        });
    }

    public void finishActivity() {
        Log.i("EditExchangeRate", "Activity finished");
        finish();
    }
}
